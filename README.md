# Proyecto Subneteo Redes y Comunicaciones #

Andres Felipe Jimenes, Mateo Montoya Pachon

# Analisis Topologia#

Podemos Evidenciar, que en la topologia de la imagen numero 1 dada en el taller de clase existen :


 5 Routers ubicados en la parte superior de la imagen conectados a traves de 7 WANS 
 
 6 Switches ubicados debajo de los routers 3 en la parte izquierda los cuales representan la LAN de bogota y 3 ubicados en la parte 
 derecha los cuales representan la LAN España
 
 7 Wans
 
 2 lans
 
 3 Protocolos : DHCP/NAT/RIP
 
 Protocolo DHCP : es un protocolo el cual permite asignar una dirección IP de manera automatica, una direccion individual
 a cada equipo que quiera conectarse a una red esto es lo que permite el portocolo DHCP.
 
 Protocolo NAT : El protocolo NAT es el protocolo el cual permite compartir la conexion de internet en una red local, normalmente
 las direcciones IP privadas no pueden enrutarse a Internet, al implementar este protocolo permite que las maquinas puedan acceder a
 internet.
 
 Protocolo RIP: El protocolo RIP se usa para administrar informacion en una red autocontenida, tal como una LAN Corporativa o una 
 WAN Privada, con el uso de este protocolo el host de puerta de enlace envia las tablas de enrutamiento en un tiempo de cada 30 segundos.
 

Para la conexion entre los routers (WAN) Se requirieron un total de 14 subredes al ser 7 WANS en total 


Para la LAN de Bogota se requierieron de 6 Subredes


Para la LAN de España se requirieron de 3 Subredes 

Para un TOTAL de 23 Subredes 


REDES 	ESPACIO DE DIRECCIONES


LAN_BOGOTA	172.41.0.0/16


LAN_ESPAÑA	10.121.0.0/16


WAN	190.85.221.0/24


Se necesitan 2 host por cada uno de los 7 enlaces WAN que se encuentran en la topología de red de la empresa, por lo tanto:

1.	2 = 1 0 = 2 bits para reservar

2.	La red tiene la máscara de red por defecto = /24

11111111.11111111.11111111.00000000

255.255.255.0

Al asignar los bits para reservar la máscara seria la siguiente

11111111.11111111.11111111.11111100 = /30

255.255.255.252

Cantidad de subredes = 2^6=64

Cantidad de host por subred 2^2-2=2

Cantidad de incremento ultimo bit encendido = 2^2 = 4

3.	Así que los rangos de direcciones para los 7 enlaces y las 14 interfaces sin contar las conexiones de los servidores son las siguientes para cumplir que cada interfaz tenga una dirección IP:

1)	190.85.221.0 – 190.85.221.3           R1_BOG – ISP_BOG

2)	190.85.221.4 – 190.85.221.7           ISP_BOG - FL

3)	190.85.221.8 – 190.85.221.11        ISP_BOG - NET

4)	190.85.221.12 – 190.85.221.15	    FL - NET

5)	190.85.221.16 – 190.85.221.19     FL – ISP_Esp

6)	190.85.221.20 – 190.85.221.23	    NET – ISP_Esp

7)	190.85.221.24 – 190.85.221.27	    ISP_Esp – R2_Esp

8)	……………………………………………

64) 190.85.221.252 - 190.85.221.255 

Con el Subneteo quedarían disponibles las direcciones:

Desde 190.85.221.28 hasta 190.85.221.255 para futuras conexiones de la red WAN



Para LAN BOGOTÁ se requieren 950 host por cada subred, esta es una topología de árbol. 

Se requieren 950 host, por lo tanto:

1.	950 = 1110110110 = 10 bits

2.	La red tiene la máscara de red por defecto = /16

11111111.11111111.00000000.00000000

255.255.0.0

Al asignar los bits para reservar la máscara seria la siguiente

11111111.11111111.11111100.00000000 = /22

255.255.252.0

Cantidad de subredes = 2^6 = 64

Cantidad de host por subred = 2^10-2 = 1022

Cantidad de incremento ultimo bit encendido = 2^2 = 4

3.	Los rangos de direcciones para los hosts por cada subred quedarían de la siguiente manera:

1)	172.41.0.0 – 172.41.3.255  Usuarios Ingeniería

2)	172.41.4.0 – 172.41.7.255  Usuarios Tecnología 

3)	172.41.8.0 – 172.41.11.255 Usuarios Gestión

4)	172.41.12.0 -172.41.15.255 Subredes disponibles para mas sectores de la empresa

5)	172.41.16.0 – 172.41.19.255

6)	……………………………………………

64) 172.41.252.0 – 172.41.255.255 Ultimo rango de direcciones 


Para las LAN ESPAÑA se requieren 450 host por cada subred. 

Para el Subneteo de los hosts requeridos se tiene lo siguiente:

1.	450 = 111000010 = 9 bits

2.	La red tiene la máscara de red por defecto = /16

11111111.11111111.00000000.00000000

255.255.0.0

Al asignar los bits para reservar la máscara seria la siguiente

11111111.11111111.11111110.00000000 = /23

255.255.254.0

Cantidad de subredes = 2^7 = 128

Cantidad de host por subred = 2^9-2 = 512

Cantidad de incremento ultimo bit encendido = 2^1 = 2

3.	Los rangos de direcciones para los hosts por cada subred quedarían de la siguiente manera:


1)	10.121.0.0 – 10.121.1.255 Usuarios Tesorería

2)	10.121.2.0 – 10.121.3.255 Usuarios Vicepresidencia

3)	10.121.4.0 – 10.121.5.255 Usuarios Gestión

4)	10.121.6.0 – 10.121.7.255 Subredes disponibles para más sectores de la empresa

5)	10.121.8.0 – 10.121.9.255

6)	……………………………………….

128) 10.121.254.0 – 10.121.255.255 Ultimo rango de direcciones

 

#Citaciones#

El Dhcp y la configuración de redes. IONOS Digitalguide. (n.d.). Retrieved October 25, 2021, from https://www.ionos.es/digitalguide/servidores/configuracion/que-es-el-dhcp-y-como-funciona/. 

Protocolo Nat Nat. Windows Server 2012 - Las bases imprescindibles para administrar y configurar su servidor - Protocolo NAT | Editiones ENI. (n.d.). Retrieved October 25, 2021, from https://www.ediciones-eni.com/open/mediabook.aspx?idR=cfbc9b5530b066096c642012f434488c. 

Acerca del Protocolo de Infromacion de Enrutamiento (RIP). Acerca del Protocolo de Información de Enrutamiento (rip y ripng). (n.d.). Retrieved October 25, 2021, from https://www.watchguard.com/help/docs/fireware/12/es-419/Content/es-419/dynamicrouting/rip_about_c.html. 

YouTube. (n.d.). QK96 TV. YouTube. Retrieved October 26, 2021, from https://www.youtube.com/c/QK96TV. 

Custom URL shortener, Link Management &amp; branded links. Bitly. (2021, October 19). Retrieved October 26, 2021, from https://bitly.com/. 